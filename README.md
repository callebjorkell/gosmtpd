# A very simple SMTP server with a REST API

[![build status](https://gitlab.com/sorenmat/gosmtpd/badges/master/build.svg)](https://gitlab.com/sorenmat/gosmtpd/commits/master)
[![Go Report Card](https://goreportcard.com/badge/gitlab.com/sorenmat/gosmtpd)](https://goreportcard.com/report/gitlab.com/sorenmat/gosmtpd)

A server that accepts smtp request and saves the emails in memory for later retrieval.

## Usage
```shell
usage: gosmtpd [<flags>]

Flags:
  --help                  Show context-sensitive help (also try --help-long and --help-man).
  --webport="8000"        Port the web server should run on
  --hostname="localhost"  Hostname for the smtp server to listen to
  --port="2525"           Port for the smtp server to listen to
  --forwardhost=""        The hostname after the @ that we should forward i.e. gmail.com
  --forwardsmtp=""        SMTP server to forward the mail to
  --forwardport="25"      The port on which email should be forwarded
  --forwarduser=""        The username for the forward host
  --forwardpassword=""    Password for the user
  --mailexpiration=300    Time in seconds for a mail to expire, and be removed from database
  --version               Show application version.
``` 

## GET /status
Returns a 200 if the service is up

## GET /mail 
List all mails

## GET /inbox/:email 
List all email for a given email address

## GET /email/:id 
Get an email by id

## DELETE /inbox/:email 
Delete all mails for a given email

## DELETE /email/:id 
Delete a email via the id

# Trying it out

You can install it by doing 

``docker push registry.gitlab.com/sorenmat/gosmtpd``


``docker start registry.gitlab.com/sorenmat/gosmtpd``

