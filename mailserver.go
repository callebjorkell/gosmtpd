package main

import (
	"log"
	"strings"
	"sync"
	"time"

	"github.com/google/uuid"
)

type MailServer struct {
	hostname       string
	port           string
	forwardEnabled bool
	forwardHost    string
	forwardPort    string
	// In-Memory database
	database []MailConnection
	mu       *sync.Mutex

	httpport       string

	expireMutex *sync.Mutex
	expireinterval int

	listenersMutex *sync.Mutex
	listeners map[string]chan MailConnection
}

func (server *MailServer) registerListener(id string, listener chan MailConnection) {
	server.listenersMutex.Lock()
	if server.listeners == nil {
		server.listeners = make(map[string]chan MailConnection)
	}
	server.listeners[id] = listener
	server.listenersMutex.Unlock()
}

func (server *MailServer) removeListener(id string) {
	server.listenersMutex.Lock()
	delete(server.listeners, id)
	server.listenersMutex.Unlock()
}

func (server *MailServer) saveMail(mail *MailConnection) bool {
	server.mu.Lock()
	defer server.mu.Unlock()
	if err := isEmailAddressesValid(mail); err != nil {
		log.Printf("Email from '%v' doesn't have a valid email address the error was %v\n", mail.From, err)
		return false
	}
	mail.expireStamp = time.Now().Add(time.Duration(mail.mailserver.expireinterval) * time.Second)
	mail.Received = time.Now().Unix()
	mail.MailId = uuid.New().String()

	// Save the mail in each recipient mailbox
	for _, rcpt := range mail.recepient {
		to, err := cleanupEmail(rcpt)
		if err != nil {
			log.Printf("Cleaning up email gave an error %v\n", err)
		}
		mail.To = to
		server.database = append(server.database, *mail)
	}

	server.listenersMutex.Lock()
	for _, v := range server.listeners {
		v <- *mail // notify all listener
	}
	server.listenersMutex.Unlock()

	if *forwardhost != "" && *forwardport != "" {
		if strings.Contains(mail.To, *forwardhost) {
			forwardEmail(mail)
		}
	}

	return true
}

func (server *MailServer) cleanupDatabase() {
	server.mu.Lock()
	dbcopy := []MailConnection{}
	log.Printf("About to expire entries from database older then %d seconds, current length %d\n", server.expireinterval, len(server.database))

	for _, v := range server.database {
		if time.Since(v.expireStamp).Seconds() < 0 {
			dbcopy = append(dbcopy, v)
		}
	}
	server.database = dbcopy
	log.Println("After expire entries in database, new length ", len(server.database))
	server.mu.Unlock()
}
