package main

import (
	"encoding/json"
	"log"
	"net/http"

	"github.com/gorilla/websocket"
	"github.com/pborman/uuid"
	"github.com/zenazn/goji"
	"github.com/zenazn/goji/web"
	"strconv"
)

var upgrader = websocket.Upgrader{
	ReadBufferSize:  1024,
	WriteBufferSize: 1024,
	CheckOrigin: func(r *http.Request) bool {
		return true
	},
}

func setupWebRoutes(config *MailServer) {
	goji.Get("/status", func(c web.C, w http.ResponseWriter, r *http.Request) {
		status(config, c, w, r)
	})
	goji.Get("/mail", func(c web.C, w http.ResponseWriter, r *http.Request) {
		allMails(config, c, w, r)
	})
	goji.Delete("/mail", func(c web.C, w http.ResponseWriter, r *http.Request) {
		deleteAllMails(config, c, w, r)
	})
	goji.Get("/inbox/:email", func(c web.C, w http.ResponseWriter, r *http.Request) {
		inbox(config, c, w, r)
	})
	goji.Get("/email/:id", func(c web.C, w http.ResponseWriter, r *http.Request) {
		mailByID(config, c, w, r)
	})
	goji.Delete("/inbox/:email", func(c web.C, w http.ResponseWriter, r *http.Request) {
		deleteMails(config, c, w, r)
	})

	goji.Delete("/email/:id", func(c web.C, w http.ResponseWriter, r *http.Request) {
		deleteByID(config, c, w, r)
	})
	goji.Handle("/inbox/:email/listen", func(c web.C, w http.ResponseWriter, r *http.Request) {
		registerListener(config, c, w, r)
	})

}

// Register a listener for changes to the mail database
func registerListener(config *MailServer, c web.C, w http.ResponseWriter, r *http.Request) {
	sinceStr := r.FormValue("since")
	email := c.URLParams["email"]
	listener := make(chan MailConnection)

	conn, err := upgrader.Upgrade(w, r, nil)
	if err != nil {
		log.Println(err)
		return
	}
	id := uuid.New()
	defer func() {
		config.removeListener(id)
		conn.Close()
	}()
	config.registerListener(id, listener)

	// if sinceStr is present fetch data from database and
	// publish those first
	if sinceStr != "" {
		i, err := strconv.ParseInt(sinceStr, 10, 64)
		if err != nil {
			panic(err)
		}
		config.mu.Lock()
		for _, msg := range config.database {
			if msg.To == email && msg.Received >= i {
				err = conn.WriteJSON(msg)
				if err != nil {
					log.Println("Sending error:", err)
					return
				}
			}
		}
		config.mu.Unlock()
	}

	go func() {
		for {
			messageType, _, err := conn.ReadMessage()
			if err != nil {
				return
			}
			if messageType == websocket.CloseMessage {
				conn.Close()
				break
			}
		}
	}()
	for {
		msg := <-listener
		if msg.To == email {
			err = conn.WriteJSON(msg)
			if err != nil {
				log.Println("Sending error:", err)
				return
			}
		}
	}
}

func status(config *MailServer, c web.C, w http.ResponseWriter, r *http.Request) {
	w.Write([]byte("OK"))
}

func databaseToJSON(mails []MailConnection) []byte {
	result, err := json.MarshalIndent(mails, "", "  ")
	if err != nil {
		log.Panic(err)
	}
	return result
}

func allMails(config *MailServer, c web.C, w http.ResponseWriter, r *http.Request) {
	w.Write(databaseToJSON(config.database))
}

func inbox(config *MailServer, c web.C, w http.ResponseWriter, r *http.Request) {
	email := c.URLParams["email"]

	var result []MailConnection
	for _, msg := range config.database {
		if msg.To == email {
			result = append(result, msg)
		}
	}
	if len(result) == 0 {
		http.NotFound(w, r)
	}

	w.Write(databaseToJSON(result))
}

func mailByID(config *MailServer, c web.C, w http.ResponseWriter, r *http.Request) {
	id := c.URLParams["id"]
	found := false
	for _, msg := range config.database {
		if msg.MailId == id {
			jsonData := databaseToJSON(config.database)
			w.Write(jsonData)
			found = true
		}
	}
	if !found {
		http.NotFound(w, r)
	}
}

func deleteMails(config *MailServer, c web.C, w http.ResponseWriter, r *http.Request) {
	email := c.URLParams["email"]

	var result []MailConnection
	for _, msg := range config.database {
		if msg.To != email {
			result = append(result, msg)
		}
	}
	config.database = result
}

func deleteAllMails(config *MailServer, c web.C, w http.ResponseWriter, r *http.Request) {
	config.database = []MailConnection{}
}

func deleteByID(config *MailServer, c web.C, w http.ResponseWriter, r *http.Request) {
	id := c.URLParams["id"]

	var result []MailConnection
	for _, msg := range config.database {
		if msg.MailId != id {
			result = append(result, msg)
		}
	}
	config.database = result
}
