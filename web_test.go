package main

import (
	"golang.org/x/net/websocket"
	"log"
	"testing"
	"time"
	"strconv"
	"encoding/json"
)

// While listing to the web socket we should only receive mails we have subscribed on.
func TestWebSocket(t *testing.T) {
	email := "sorenm@test.com"
	ws, err := websocket.Dial("ws://localhost:8000/inbox/" + email + "/listen", "", "http://localhost")
	if err != nil {
		log.Fatal(err)
	}

	sendMail(email)

	var message string
	websocket.Message.Receive(ws, &message)
	mc := &MailConnection{}
	json.Unmarshal([]byte(message), mc)

	if mc.To != email {
		t.Error("Email contains wrong 'to' address")
	}
	if mc.From != "sorenm@mymessages.dk" {
		t.Error("Email contains wrong 'from' address")
	}

}

// While listing to the web socket we should only receive mails we have subscribed on.
// We should not see email for other addresses
func TestWebSocket_filter(t *testing.T) {
	email := "sorenm@test.com"
	ws, err := websocket.Dial("ws://localhost:8000/inbox/" + email + "/listen", "", "http://localhost")
	if err != nil {
		log.Fatal(err)
	}

	sendMail("joe@void.com")
	sendMail(email)
	sendMail("joe@void1.com")

	var message string
	websocket.Message.Receive(ws, &message)
	mc := &MailConnection{}
	json.Unmarshal([]byte(message), mc)

	if mc.To != email {
		t.Error("Email contains wrong 'to' address")
	}
	if mc.From != "sorenm@mymessages.dk" {
		t.Error("Email contains wrong 'from' address")
	}

}

// Listen to websocket, but don't send any mails.
// This should result in a read time out
func TestWebSocket_NoSend(t *testing.T) {
	email := "sorenm@test.com"
	ws, err := websocket.Dial("ws://localhost:8000/inbox/" + email + "/listen", "", "http://localhost")
	if err != nil {
		log.Fatal(err)
	}

	var message string
	ws.SetReadDeadline(time.Now().Add(2 * time.Second))
	err = websocket.Message.Receive(ws, &message)

	if err == nil {
		t.Error("Get a message, expected none. ", err)
	}

	if message != "" {
		t.Error("Get a message, expected none")
	}
}

// While listing to the web socket we should only receive mails we have subscribed on.
func TestWebSocket_Since(t *testing.T) {
	email := "sorenm@test.com"
	beforeMails := time.Now().Unix()
	sendMailWithBody(email, "Mail 1")
	sendMailWithBody(email, "Mail 2")
	sendMailWithBody(email, "Mail 3")

	ws, err := websocket.Dial("ws://localhost:8000/inbox/" + email + "/listen?since=" + strconv.FormatInt(beforeMails, 10), "", "http://localhost")
	if err != nil {
		log.Fatal(err)
	}

	var message string
	websocket.Message.Receive(ws, &message)
	mc := &MailConnection{}
	json.Unmarshal([]byte(message), mc)

	if mc.To != email {
		t.Error("Email contains wrong 'to' address")
	}
	if mc.From != "sorenm@mymessages.dk" {
		t.Error("Email contains wrong 'from' address")
	}

}